<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'password' => 'required'
        ];
    }

    public function getCredentials(){
        //aquí usando la función isEmail si username es en realidad
        //el email lo aceptamos igualmente
        $username = $this->get('username');

        if($this->isEmail($username)){
            return [
                'email' => $username,
                'password' => $this->get('password')
            ];
        }
        //solo regresamos email y password
        //o username y password
        return $this->only('username', 'password');
    }

    public function isEmail($value){
        //Usamos la clase con alias ValidationFactory para hacer que el usuario
        //pueda meter email o username indistintamente
        $factory = $this->container->make(ValidationFactory::class);
        //1er parametro username=$value, 2o parametro comparamos si username = email
        //esto me devuelve un objeto de tipo Validator que con el método fails y la
        //exclamación retorna false en la función si no es un email
        return !$factory->make(['username' => $value], ['username' => 'email'])->fails();
    }
}
