<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //cambiamos a true porque queremos que por defecto
        //se acepten solicitudes
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //users es el nombre de la tabla, email el del campo
            'email' => 'required|unique:users,email',
            //si en bd encuentra un nombre igual va a descartar la solicitud
            'username' => 'required|unique:users,username',
            'password' => 'required|min:8',
            //same:password significa que este campo debe ser igual que el de password
            'password_confirmation' => 'required|same:password',
        ];
    }
}
