<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        //Podemos obtener info del usuario que está actualmente logeado
        //Auth::user()->id;
        return view('home.index');
    }
}
