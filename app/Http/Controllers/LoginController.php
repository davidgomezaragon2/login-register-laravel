<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
//Clase para manejar autenticación
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function show(){
        //Esto automáticamente valida si hay un usuario con una sesión autenticada
        //De esta forma el usuario logeado no vuelve al login porque yo no es necesario
        //y el que no está logeado se va a tomar por el orto
        //Lo mismo haremos con register por ejemplo
        if(Auth::check()){
            return redirect('/home');
        }
        return view('auth.login');
    }

    public function login(LoginRequest $request){
        //Aquí obtenemos email y password o username y password
        $credentials = $request->getCredentials();
        //Aquí usamos la clase Auth importada
        if(!Auth::validate($credentials)){
            //otra forma distinta de hacer redirect que la que tenemos en
            //la función register de RegisterController
            return redirect()->to('/login')->withErrors('Username and/or password is incorrect');
        }
        //vamos a crear un usuario a través de la autenticación de las credenciales
        //que recibimos de la solicitud http
        $user = Auth::getProvider()->retrieveByCredentials($credentials);
        //función para logearse
        Auth::login($user);

        return $this->authenticated($request, $user);
    }
    //en principio no vamos a necesitar $user
    public function authenticated(Request $request, $user){
        return redirect('/home');
    }
}
