<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//importamos RegisterRequest y User
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function show(){
        if(Auth::check()){
            return redirect('/home');
        }
        return view('auth.register');
    }

    public function register(RegisterRequest $request){
        //solo va a crear la instancia de User si los datos están validados
        //esta validacion esta configurada en RegisterRequest 
        $user = User::create($request->validated());
        return redirect('/login')->with('success', 'Account created successfully');
    }
}
