@extends('layouts.app-master')

@section('content')

    <h1 class="container">Home</h1>
    <!--Con estas etiquetas de auth y guest laravel distinguen entre
    usuarios autenticados e invitados y muestra un contenido distinto
    para cada uno de ellos-->
    @auth
        <!--Ojo a como sacamos datos del usuario autenticado-->
        <!--p>Bienvenido {{auth()->user()->username}}, estás autenticado a la página</p-->
        <!--Si existe name mostramos name y si no username-->
        <p>Bienvenido {{auth()->user()->name ?? auth()->user()->username}}, estás autenticado a la página</p>
        <a href="/logout">Logout</a>
    @endauth
    <!--Si intento entrar en /home sin estar logeado me sale esto-->
    @guest
        <p>Para ver el contenido <a href="/login">Inicia sesión</a></p>
    @endguest

@endsection