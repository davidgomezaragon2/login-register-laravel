<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aplicación de Login</title>
    <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css')}}">
</head>
<!--esta vista la hacemos porque no necesariamente el usuario va a ver lo mismo
cuando va a registrarse o iniciar sesión que cuando ya está dentro aunque ya sería
cosa nuestra hacer cambios aquí-->
<body>
    @include('layouts.partials.navbar')
    <main class="container">
        @yield('content')
    </main>

    <script src="{{url('assets/js/bootstrap.bundle.min.js')}}">

    </script>
</body>
</html>