@extends('layouts.auth-master')

@section('content')
    <form action="/login" method="POST">
    @csrf
    <h1>Login</h1>
    @include('layouts.partials.messages')
        <div class="mb-3">
            <label for="exampleInputEmail1">Username / Email address</label>
            <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"><br>
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>
        <div class="mb-3">
            <a href="/register">Create account</a>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
